package com.design.pattern.strategy;

/**
 * @author @追风少年
 * @description 策略接口
 * @date 2022/4/23
 */
public interface StrategyPatternService {

    /**
     * 根据具体的ActionName执行具体的Action
     * @return
     */
    String getActionName();

    /**
     * 执行的具体的action
     */
    void doAction();

}
