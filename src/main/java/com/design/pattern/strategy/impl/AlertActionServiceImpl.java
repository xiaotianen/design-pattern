package com.design.pattern.strategy.impl;

import com.design.pattern.strategy.StrategyPatternService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author @追风少年
 * @description alert
 * @date 2022/4/23
 */
@Slf4j
@Service
public class AlertActionServiceImpl  implements StrategyPatternService {

    @Override
    public String getActionName() {
        return "alert";
    }

    @Override
    public void doAction() {
        // 执行具体的Action
        log.warn("alert!");

    }
}
