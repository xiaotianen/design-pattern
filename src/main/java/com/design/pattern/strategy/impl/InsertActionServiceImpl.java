package com.design.pattern.strategy.impl;

import com.design.pattern.strategy.StrategyPatternService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author @追风少年
 * @description insert
 * @date 2022/4/23
 */
@Slf4j
@Service
public class InsertActionServiceImpl implements StrategyPatternService {

    @Override
    public String getActionName() {
        return "insert";
    }

    @Override
    public void doAction() {
        log.warn("insert");
    }
}
