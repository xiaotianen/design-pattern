package com.design.pattern.template;

import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;

/**
 * @author @追风少年
 * @description
 * @date 2022/4/23
 */
@Component
public class FuzzySearchConverter extends FormItemConverter {

    @Override
    public String getType() {
        return "FUZZY_SEARCH";
    }

    @Override
    protected void afterItem(JSONObject item) {
        System.out.println("创建之后进行。。。");
    }
}
