package com.design.pattern.template;

import com.alibaba.fastjson.JSONObject;

/**
 * @author @追风少年
 * @description FormItemConverter
 * @date 2022/4/23
 */
public abstract class FormItemConverter {

    public abstract String getType();

    public final String convert(JSONObject config) {
        JSONObject item = createItem("123");

        afterItem(item);

        lastItem(item);

        return "item";
    }

    protected void afterItem(JSONObject item) {}

    private JSONObject createItem(String config) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("test", config);

        return jsonObject;
    }

    protected void lastItem(JSONObject item) {}
}
