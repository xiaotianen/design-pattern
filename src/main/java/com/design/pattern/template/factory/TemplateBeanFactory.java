package com.design.pattern.template.factory;

import com.design.pattern.template.FormItemConverter;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author @追风少年
 * @description 工厂
 * @date 2022/4/23
 */
@Component
public class TemplateBeanFactory implements ApplicationContextAware {

    private final Map<String, FormItemConverter> formItemConverterMap = new HashMap<>();

    public FormItemConverter getConverter(String type) {
        return formItemConverterMap.get(type);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        Map<String, FormItemConverter> strategyServiceMap =
                applicationContext.getBeansOfType(FormItemConverter.class);

        strategyServiceMap.values().forEach(formItemConverter -> {
            formItemConverterMap.put(formItemConverter.getType(), formItemConverter);
        });
    }
}
