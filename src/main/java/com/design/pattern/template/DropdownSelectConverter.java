package com.design.pattern.template;

import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;

/**
 * @author @追风少年
 * @description
 * @date 2022/4/23
 */
@Component
public class DropdownSelectConverter extends FormItemConverter {

    @Override
    public String getType() {
        return "DROPDOWN_SELECT";
    }

    @Override
    protected void lastItem(JSONObject item) {
        System.out.println("最后....");
    }


}
