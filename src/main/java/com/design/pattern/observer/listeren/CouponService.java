package com.design.pattern.observer.listeren;

import com.design.pattern.observer.event.UserRegisterEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * @author @追风少年
 * @description 为新用户发送优惠券
 * @date 2023/3/4
 */
@Slf4j
@Service
public class CouponService {


    /**
     * 下发优惠券
     * @see com.design.pattern.observer.listeren.EmailEventListener （事件监听的另一只实现方式）
     * @param event
     */
    @EventListener
    public void issueCoupon(UserRegisterEvent event){
        log.info("用户【{}】已下发优惠券", event.getUsername());
    }

}
