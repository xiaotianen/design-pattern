package com.design.pattern.observer.listeren;

import com.design.pattern.observer.event.UserRegisterEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

/**
 * @author @追风少年
 * @description 为新注册用户发送邮箱通知
 * @date 2023/3/4
 */
@Slf4j
@Service
public class EmailEventListener implements ApplicationListener<UserRegisterEvent> {

    @Override
    public void onApplicationEvent(UserRegisterEvent event) {
        log.info("用户【{}】邮箱已发送注册通知", event.getUsername());
    }
}
