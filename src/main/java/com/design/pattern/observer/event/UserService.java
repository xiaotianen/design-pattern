package com.design.pattern.observer.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;

/**
 * @author @追风少年
 * @description 事件发布服务
 * @date 2023/3/4
 */
@Slf4j
@Service
public class UserService implements ApplicationEventPublisherAware {

    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    /**
     * 用户注册事件
     * @param username 注册用户名
     */
    public void register(String username){
        log.info("用户注册事件发生......");
        UserRegisterEvent userRegisterEvent = new UserRegisterEvent(this, username);
        // 事件推送
        applicationEventPublisher.publishEvent(userRegisterEvent);
    }
}
