package com.design.pattern.observer.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author @追风少年
 * @description 用户注册事件
 * @date 2023/3/4
 */
public class UserRegisterEvent extends ApplicationEvent {

    private String username;

    public UserRegisterEvent(Object source) {
        super(source);
    }

    public UserRegisterEvent(Object source, String userName) {
        super(source);
        this.username = userName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


}
