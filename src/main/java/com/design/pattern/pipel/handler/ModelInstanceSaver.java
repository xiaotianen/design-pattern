package com.design.pattern.pipel.handler;

import com.design.pattern.pipel.content.InstanceBuildContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author @追风少年
 * @description 处理器 - 保存模型实例到相关DB表
 * @date 2022/4/23
 */
@Slf4j
@Component
public class ModelInstanceSaver implements ContextHandler<InstanceBuildContext> {

    @Override
    public boolean handle(InstanceBuildContext context) {
        log.info("--保存模型实例到相关DB表--");

        // 假装保存模型实例

        return true;
    }
}
