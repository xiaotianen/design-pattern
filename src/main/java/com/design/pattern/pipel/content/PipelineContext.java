package com.design.pattern.pipel.content;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author @追风少年
 * @description 传递到管道的上下文
 * @date 2022/4/23
 */
@Getter
@Setter
public class PipelineContext {

    /**
     * 处理开始时间
     */
    private LocalDateTime startTime;

    /**
     * 处理结束时间
     */
    private LocalDateTime endTime;

    /**
     * 获取数据名称
     */
    public String getName() {
        return this.getClass().getSimpleName();
    }
}
