package com.design.pattern.pipel;

import com.design.pattern.pipel.content.InstanceBuildContext;
import com.design.pattern.pipel.executor.PipelineExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author @追风少年
 * @description
 * @date 2022/4/23
 */
@Slf4j
@Service
public class ModelServiceImpl implements ModelService {

    /**
     * 提交模型（构建模型实例）,未应用管道模式前的代码
     */
    //public CommonReponse<Long> buildModelInstance(InstanceBuildRequest request) {
    //    // 输入数据校验
    //    validateInput(request);
    //    // 根据输入创建模型实例
    //    ModelInstance instance = createModelInstance(request);
    //    // 保存实例到相关 DB 表
    //    saveInstance(instance);
    //}


    @Resource
    private PipelineExecutor pipelineExecutor;

    /**
     * 应用管道模式后的改进
     * @return
     */
    @Override
    public Long buildModelInstance() {
        InstanceBuildContext data = createPipelineData();
        boolean success = pipelineExecutor.acceptSync(data);

        // 创建模型实例成功
        if (success) {
            return data.getInstanceId();
        }

        log.error("创建模式实例失败：{}", data.getErrorMsg());
        return -1L;
    }

    private InstanceBuildContext createPipelineData() {
        InstanceBuildContext instanceBuildContext = new InstanceBuildContext();
        instanceBuildContext.setInstanceId(1L);
        instanceBuildContext.setModelId(2L);
        Map<String, Object> formInput = new HashMap<>();
        formInput.put("instanceName", "@追风少年");
        instanceBuildContext.setFormInput(formInput);
        return instanceBuildContext;
    }
}
