package com.design.pattern.pipel.executor;

import com.design.pattern.pipel.content.InstanceBuildContext;
import com.design.pattern.pipel.content.PipelineContext;
import com.design.pattern.pipel.handler.ContextHandler;
import com.design.pattern.pipel.handler.InputDataPreChecker;
import com.design.pattern.pipel.handler.ModelInstanceCreator;
import com.design.pattern.pipel.handler.ModelInstanceSaver;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author @追风少年
 * @description 管道路由的配置
 * <p>
 *     提前写好一份路由表，指定好 ”Context -> 管道“ 的映射（管道用 List<ContextHandler> 来表示），以及管道中处理器的顺序 。
 *     Spring 来根据这份路由表，在启动时就构建好一个 Map，Map 的键为 Context 的类型，值为 管道（即 List<ContextHandler>）。
 *     这样的话，如果想知道每个管道的处理链路，直接看这份路由表就行，一目了然。
 *     缺点嘛，就是每次加入新的 ContextHandler 时，这份路由表也需要在对应管道上进行小改动
 * </p>
 * @date 2022/4/23
 */
@Configuration
public class PipelineRouteConfig implements ApplicationContextAware {

    private ApplicationContext appContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        appContext = applicationContext;
    }

    /**
     * 数据类型->管道中处理器类型列表 的路由
     */
    private static final
    Map<Class<? extends PipelineContext>,
            List<Class<? extends ContextHandler<? extends PipelineContext>>>>
            PIPELINE_ROUTE_MAP = new HashMap<>(4);

    /*
     * 在这里配置各种上下文类型对应的处理管道：键为上下文类型，值为处理器类型的列表
     */
    static {
        PIPELINE_ROUTE_MAP.put(InstanceBuildContext.class,
                                    Arrays.asList(
                                            InputDataPreChecker.class,
                                            ModelInstanceCreator.class,
                                            ModelInstanceSaver.class
                                    )
        );

        // 将来其他 Context 的管道配置
    }

    /**
     * 在 Spring 启动时，根据路由表生成对应的管道映射关系
     */
    @Bean("pipelineRouteMap")
    public Map<Class<? extends PipelineContext>, List<? extends ContextHandler<? extends PipelineContext>>> getHandlerPipelineMap() {
        return PIPELINE_ROUTE_MAP.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, this::toPipeline));
    }

    /**
     * 根据给定的管道中 ContextHandler 的类型的列表，构建管道
     */
    private List<? extends ContextHandler<? extends PipelineContext>> toPipeline(
            Map.Entry<Class<? extends PipelineContext>, List<Class<? extends ContextHandler<? extends PipelineContext>>>> entry) {
        return entry.getValue()
                .stream()
                .map(appContext::getBean)
                .collect(Collectors.toList());
    }


}
