package com.design.pattern.pipel;

/**
 * @author @追风少年
 * @description 业务入口
 * @date 2022/4/23
 */
public interface ModelService {

    Long buildModelInstance();
}
