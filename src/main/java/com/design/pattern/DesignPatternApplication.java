package com.design.pattern;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * 管道模式：<a href="https://mp.weixin.qq.com/s?__biz=MzAxNDEwNjk5OQ==&mid=2650413430&idx=1&sn=32c89ea3222d341bf3014854f69fd239&chksm=8396d16eb4e1587838f99a6859668a3588b0762155c1a13872d3f419efa3f75c5a532d7d38fe&scene=178&cur_album_id=1452661944472977409#rd">基于 Spring 实现管道模式的最佳实践</a>
 * 策略模式：<a href="">策略模式</a>
 * 模板模式：<a href="">模板模式</a>
 */
@SpringBootApplication
@ImportResource(value = {"classpath:spring-bean.xml"})
public class DesignPatternApplication {

    public static void main(String[] args) {
        SpringApplication.run(DesignPatternApplication.class, args);
    }

}
